﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class MergedDocument
    {
        public string Document { get; set; }
    }

    public class LoadedDocument
    {
        public string DocumentData { get; set; }
        public string DocumentName { get; set; }
    }
}
