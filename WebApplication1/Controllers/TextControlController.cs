﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{


    [ApiController]
    [Route("[controller]")]
    public class TextControlController : ControllerBase
    {
        [HttpPost]
        [Route("SaveDocument")]
        public MergedDocument SaveDocument([FromBody] MergedDocument Document)
        {
            try
            {
                byte[] data;
                using (TXTextControl.ServerTextControl tx = new TXTextControl.ServerTextControl())
                {
                    tx.Create();
                    tx.Load(Convert.FromBase64String(Document.Document), TXTextControl.BinaryStreamType.InternalUnicodeFormat);
                    tx.Save("App_Data/" + Guid.NewGuid().ToString() + ".docx", TXTextControl.StreamType.WordprocessingML);
                    return new MergedDocument() { Document = Document.Document };
                }
            } 
            catch (Exception ex)
            {

            }
            return new MergedDocument();
        }

        [HttpGet]
        [Route("LoadDocumentForWrite/{docname}")]
        public MergedDocument LoadDocumentForWrite(string docname)
        {
            try
            {
                using (TXTextControl.ServerTextControl tx = new TXTextControl.ServerTextControl())
                {
                    tx.Create();
                    tx.Load("App_Data/" + docname, TXTextControl.StreamType.WordprocessingML);
                    byte[] data;
                    tx.Save(out data, TXTextControl.BinaryStreamType.InternalUnicodeFormat);
                    tx.Clear();
                    
                    return new MergedDocument() { Document = Convert.ToBase64String(data) };
                }
            }
            catch (Exception ex)
            {
                return new MergedDocument();
            }

        }

        [HttpGet]
        [Route("LoadDocumentForRead/{docname}")]
        public LoadedDocument LoadDocumentForRead(string docname)
        {
            using (TXTextControl.ServerTextControl tx = new TXTextControl.ServerTextControl())
            {
                tx.Create();
                byte[] data;
                string documentName = docname;//"invoice.docx";
                tx.Load("App_Data/" + docname, TXTextControl.StreamType.WordprocessingML);
                tx.Save(out data, TXTextControl.BinaryStreamType.InternalUnicodeFormat);
                LoadedDocument document = new LoadedDocument()
                {
                    DocumentData = Convert.ToBase64String(data),
                DocumentName = documentName
                    };
            return document;
        }
        }

    }
}
