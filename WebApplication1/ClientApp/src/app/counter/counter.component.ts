

import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

declare const loadDocument: any;
declare const saveDocument: any;
declare const loadData: any;

@Component({
  selector: 'app-counter-component',
  templateUrl: './counter.component.html',
})

export class CounterComponent {
  public _http: HttpClient;
  public _baseUrl: string;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._http = http;
    this._baseUrl = baseUrl;
  }

  async onClickLoadDocument(documentName: string) {
    // get a document from the Web API endpoint 'LoadDocument'
    this._http.get<any>(this._baseUrl + 'textcontrol/LoadDocumentForWrite/invoices.docx').subscribe(result => {
      loadDocument(result);
    }, error => console.error(error));
  }

  async onClickSaveDocument() {

    // get the saved document from TXTextControl
    let postDocument: MergedDocument = {
      document: await saveDocument(),
    };

    // post the document to endpoint 'SaveDocument'
    this._http.post<MergedDocument>(this._baseUrl + 'textcontrol/SaveDocument', postDocument).subscribe(result => {

      // load the results into TXTextControl
      loadDocument(result.document);

    }, error => console.error(error));

  }
}

interface MergedDocument {
  document: string;
}

