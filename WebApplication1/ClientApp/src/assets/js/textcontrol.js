
function loadDocument(document) {
  TXTextControl.loadDocument(TXTextControl.streamType.InternalUnicodeFormat, document.document);
}

  function saveDocument() {
    return new Promise(resolve => {
      TXTextControl.saveDocument(TXTextControl.streamType.InternalUnicodeFormat, function (e) {
        resolve(e.data);
      });
    });
  }

function loadData(JsonData) {
  console.log(JsonData);
    TXTextControl.addEventListener("textControlLoaded", function () {
    /* TXTextControl.loadJsonData(JSON.stringify(JsonData.documentData));*/
      TXTextControl.loadDocument(TXTextControl.streamType.InternalUnicodeFormat, JsonData);
    });
  }


function loadReadDocument(Document) {
    TXDocumentViewer.loadDocument(Document.documentData, Document.documentName);
  }

/* use this method if you need to load the document when open a page and not when clicking on a button*/
  var documentLoading;

  function loadInitialDocument(Document) {

    documentLoading = true;

    window.addEventListener("documentViewerLoaded", function () {

      if (documentLoading === true)
        loadReadDocument(Document);

      documentLoading = false;
    });
  }

